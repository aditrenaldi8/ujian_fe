import { UjianPage } from './app.po';

describe('ujian App', () => {
  let page: UjianPage;

  beforeEach(() => {
    page = new UjianPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
