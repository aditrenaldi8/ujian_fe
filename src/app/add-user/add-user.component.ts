import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private api:ApiService) { }

  UserList: object[];

  ngOnInit() {
    this.api.UserList = this.UserList;
  }

  name: string="";
  email: string="";
  address: string="";
  phone: string="";
  company: string="";

  addUser(){
    this.api.addData({
        "id": 1,
        "name": this.name, 
        "username": "",
        "email": this.email,
        "address": {
            "street": this.address,
            "suite": "",
            "city": "",
            "zipcode": "",
            "geo": {
                "lat": "",
                "lng": ""
            }
        },
        "phone": this.phone,
        "website": "",
        "company": {
            "name": this.company,
            "catchPhrase": "",
            "bs": ""
        }
    });

    this.name ="";
    this.email ="";
    this.address ="";
    this.phone ="";
    this.company ="";
  }

}
