import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private api:ApiService) { }
  
  UserList: object[];

  ngOnInit() {
    this.api.getData()
    .subscribe(result => this.UserList = result);
    this.api.UserList = this.UserList;
  }

  remove(index){
    this.UserList.splice(index,1);
  }

}
